//
// Everything changable you can find by text
// "Can be changed"
// All is commented, be careful in changing other values
//
var search, canvas, ctx, request;
var hovers, texts, requsetValues, sites;

function pageLoad() {
    // Can be changed
    // Setting search engines
    requestValues = [
	"https://searx.me/search?=&q=",
	"https://www.google.com/search?=&q=",
	"https://yandex.ru/images/search?text=",
	"https://yandex.ru/video/search?text=",
	"https://www.youtube.com/results?search_query="
    ];
    // Can be changed
    // Setting default site page
    sites = [
	"https://searx.me",
	"https://google.com",
	"https://yandex.ru/images",
	"https://yandex.ru/videos",
	"https://youtube.com"
    ];
    // Can be changed
    // Setting names on buttons of search engines
    texts = [
	"      searx.me",
	"    Google.com",
	" Yandex images",
	" Yandex videos",
	"   YouTube.com"
    ];
    request = requestValues[0];

    // Setting search block
    search = document.getElementById('sch');
    // Can be changed
    // Color of text in search bar
    search.style.color = "#ffffff";
    // Can be changed
    // Color of search bar's background
    search.style.backgroundColor = "#202020";
    // Can be changed
    // Color of hovered search bar's border
    search.onmouseover = function () { this.style.borderColor = '#000000'; }
    // Can be changed
    // Default color of search bar's border
    search.onmouseout = function () { this.style.borderColor = '#121212'; }
    
    // Setting header
    hovers = [false,false,false,false,false];
    canvas = document.getElementById('painting');
    ctx = canvas.getContext("2d");
    // Can be changed
    // Color of button's border
    ctx.strokeStyle = "#080808";
    // Can be changed
    // Width of button's border
    ctx.lineWidth = 10;
    // Can be changed
    // Height and type of font
    // Be careful in changing this
    ctx.font = "22px Arial";
    canvas.addEventListener("mousemove", on_move, false);
    canvas.addEventListener("click", on_click, false);
    // Drawing header
    painting();
}

function keyPress(e) {
    if (e.keyCode == 13 && search.value.length > 0) {
	window.open(request + search.value, "_self");
	return false;
    }
    else {
	//search.value += e.code;
	return true;
    }
}

// On moving over buttons checking and changing array "hovers" values
// Redrawing header
function on_move(e) {
    var x = e.layerX, y = e.layerY;
    // Checking FireFox browser
    if (typeof InstallTrigger !== 'undefined') {
  	x-=canvas.offsetLeft;
	y-=canvas.offsetTop;
    }

    if ((x>=25 && y>=0 && x+y>=85) && (x<=275 && y<=60 && x+y<=285))
	hovers = [true,false,false,false,false];
    else if ((x>=225 && y>=0 && x+y>=285) && (x<=475 && y<=60 && x+y<=485))
	hovers = [false,true,false,false,false];
    else if ((x>=425 && y>=0 && x+y>=485) && (x<=675 && y<=60 && x+y<=685))
	hovers = [false,false,true,false,false];
    else if ((x>=625 && y>=0 && x+y>=685) && (x<=875 && y<=60 && x+y<=885))
	hovers = [false,false,false,true,false];
    else if ((x>=825 && y>=0 && x+y>=885) && (x<=1075 && y<=60 && x+y<=1085))
	hovers = [false,false,false,false,true];
    else
	hovers = [false,false,false,false,false];
    painting()
}

// On clicking on button checking array "hovers" and changing "request" value
// Repainting header
function on_click(e) {
    if (hovers[0] && request == requestValues[0])
	window.open(sites[0], "_self");
    else if (hovers[0])
	request = requestValues[0];
    else if (hovers[1] && request == requestValues[1])
	window.open(sites[1], "_self");
    else if (hovers[1])
	request = requestValues[1];
    else if (hovers[2] && request == requestValues[2])
	window.open(sites[2], "_self");
    else if (hovers[2])
	request = requestValues[2];
    else if (hovers[3] && request == requestValues[3])
	window.open(sites[3], "_self");
    else if (hovers[3])
	request = requestValues[3];
    else if (hovers[4] && request == requestValues[4])
	window.open(sites[0], "_self");
    else if (hovers[4])
	request = requestValues[4];
    painting();
}

// Drawing all parallelograms
function painting() {
    drawPlm(75,5,0);
    drawPlm(275,5,1);
    drawPlm(475,5,2);
    drawPlm(675,5,3);
    drawPlm(875,5,4);
}

// Drawing parallelogram
function drawPlm(x, y, index) {
    var length = 200, height = 50, shift = 50;
    if (request == requestValues[index])
	// Can be changed
	// Color - if button active
	ctx.fillStyle = "#606060"
    else if (hovers[index])
	// Can be changed
	// Color - if button hovered
	ctx.fillStyle = "#404040";
    else
	// Can be changed
	// Default color of button
	ctx.fillStyle = "#202020";
    ctx.beginPath();
    ctx.moveTo(x,y);
    ctx.lineTo(x+length, y);
    ctx.lineTo(x+length-shift, y+height);
    ctx.lineTo(x-shift, y+height);
    ctx.closePath();
    ctx.stroke();
    ctx.fill();
    // Can be changed
    // Color of button's  text
    ctx.fillStyle="#dddddd"
    ctx.fillText(texts[index], x-10, y+32);
}
